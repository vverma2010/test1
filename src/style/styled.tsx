import { Label, styled } from '@icstark/ui'

export const Section = styled.section`
  width: 100%;
  position: relative;
  box-sizing: border-box;
`

export const SubSection = styled.div`
  margin: 0 auto;
  transition: 0.6s linear;
  @media (min-width: 1441px) {
    transition: 0.6s linear;
    width: 70%;
  }
  @media (max-width: 1440px) {
    transition: 0.6s linear;
    width: 90%;
  }
  @media (max-width: 1200px) {
    transition: 0.6s linear;
    width: 85%;
  }
  @media (max-width: 786px) {
    transition: 0.6s linear;
    width: 90%;
  }
  @media (max-width: 540px) {
    transition: 0.6s linear;
    width: 95%;
  }
`

export const H2Heading = styled(Label)`
  text-align: center;
  display: block;
  margin: 40px 0 30px;
  position: relative;
  user-select: none;
  font-size: 24px;
  color: ${(props: any) => props.theme.colors.primary};
`

export const H3Heading = styled(Label)`
  text-align: center;
  display: block;
  margin: 40px 0 30px;
  position: relative;
  user-select: none;
  font-size: 18px;
  color: ${(props: any) => props.theme.colors.primary};
  ${(props: any) => props.style}
`
