import { get, post, put, del } from '@icstark/utils'

export const getAuthorized = (url: string, cancelToken: any, config?: any) => {
  return get(url, cancelToken, {
    headers: { Authorization: localStorage.getItem('AuthToken') || null }
  })
}
export const postAuthorized = (url: string, data: any, cancelToken: any) => {
  return post(url, data, cancelToken, {
    headers: { Authorization: localStorage.getItem('AuthToken') || null }
  })
}
export const putAuthorized = (url: string, data: any, cancelToken: any) => {
  return put(url, data, cancelToken, {
    headers: { Authorization: localStorage.getItem('AuthToken') || null }
  })
}
export const deleteAuthorized = (url: string, cancelToken: any) => {
  return del(url, cancelToken, {
    headers: { Authorization: localStorage.getItem('AuthToken') || null }
  })
}
