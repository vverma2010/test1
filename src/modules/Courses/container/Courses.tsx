import { Flex, Span } from '@icstark/ui'
import React from 'react'
import { RoutesPath } from '../../../config/routes.config'
import data from '../../../data.json'
import { H2Heading, Section } from '../../../style/styled'

function Courses({ history }: any) {
  return (
    <Section>
      <H2Heading>Courses</H2Heading>
      <Flex
        justifyContentCenter
        alignItemsCenter
        wrap
        column
        width={0.9}
        style={{ margin: '0 auto' }}
      >
        {data.courseContent.map((content: any, i: number) => {
          return (
            <Flex
              wrap
              justifyContentSpaceBetween
              style={{ boxSizing: 'border-box', margin: '20px 0px', cursor: 'pointer' }}
              key={i}
              onClick={() => history.push(`${RoutesPath.CourseContent}/${content.id}`)}
            >
              <Flex
                width={[1, 0.5]}
                style={{ boxSizing: 'border-box' }}
                className="semester"
                column
                justifyContentSpaceBetween
                alignItemsCenter
              >
                <img src={content.courseImage} style={{ width: '80%', borderRadius: 10 }} />
              </Flex>
              <Flex
                width={[1, 0.5]}
                style={{ padding: 20, boxSizing: 'border-box' }}
                column
                justifyContentCenter
              >
                <Span fontSize={16} color={'#808184'} style={{ margin: 5, fontWeight: 500 }}>
                  {content.courseName}
                </Span>
                {content.registrationFee ? (
                  <Span fontSize={14} color={'#959595'} style={{ padding: '10px 20px 0' }}>
                    Registration Fee: Rs.{content.registrationFee}
                  </Span>
                ) : null}
                {content.courseFee ? (
                  <Span fontSize={14} color={'#959595'} style={{ padding: '10px 20px 0' }}>
                    Course Fee: Rs.{content.courseFee}
                  </Span>
                ) : null}
                <Span
                  fontSize={12}
                  color={'#808184'}
                  style={{ padding: '40px 5px', fontWeight: 400 }}
                >
                  Click for More Details
                </Span>
              </Flex>
            </Flex>
          )
        })}
      </Flex>
    </Section>
  )
}
export default Courses
