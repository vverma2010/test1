import React from 'react'
import { Flex, Span, Input, Button, styled } from '@icstark/ui'
import { Modal } from '@icstark/ui'
import { FaRegEnvelope, FaMobileAlt, FaWhatsapp, FaMapMarkedAlt } from 'react-icons/fa'
import { FormInput } from '../../../components/Forms/Form'
import { ContactUsContainer, ContactInfo } from '../styled'
import { validateEmail } from '../../../utility/func'
import { config } from '../../../config/config'
import { cancelToken } from '@icstark/utils'
import { postAuthorized } from '../../../services'

const style = {
  color: '#333',
  fontWeight: 400,
  marginBottom: '11px'
}

export const LabelValue = ({ label, value, icon }: any) => {
  return (
    <Flex style={style} alignItemsCenter>
      <Flex style={{ width: 50 }}>
        {icon || (
          <Span
            variant="default"
            fontSize={18}
            color={`rgba(0,0,0,0.65)`}
            style={{ fontWeight: 600 }}
          >
            {label + `:`}
          </Span>
        )}
      </Flex>
      <Flex column>
        {label === 'Address'
          ? value.split(',').map((item: any, i: any) => {
              return (
                <Span color={`#676767`} fontSize={14} key={`${i}`}>
                  {item}
                  {i < value.split(',').length - 1 && ','}
                </Span>
              )
            })
          : (
              <Span color={`#676767`} fontSize={14}>
                {value}
              </Span>
            ) || null}
      </Flex>
    </Flex>
  )
}

const Phone = styled(FaMobileAlt)`
  color: ${(props: any) => props.theme.colors.primary};
`
const Email = styled(FaRegEnvelope)`
  color: ${(props: any) => props.theme.colors.primary};
`
const WhatsApp = styled(FaWhatsapp)`
  color: ${(props: any) => props.theme.colors.primary};
`
const Map = styled(FaMapMarkedAlt)`
  color: ${(props: any) => props.theme.colors.primary};
`

function ContactUs({ setModal, modal }: any) {
  const [contact, setContact] = React.useState<any>({
    values: {
      Phone: '',
      Name: '',
      Email: '',
      Message: ''
    },
    errors: {},
    errorShow: false,
    error: false,
    msg: ''
  })
  function onchange(target: any) {
    setContact({
      ...contact,
      values: { ...contact.values, [target.name]: target.value }
    })
  }

  function onBlur({ target }: any) {
    if (target.name === 'Phone') {
      if (target.value.length !== 10) {
        setContact({
          ...contact,
          errors: { ...contact.errors, [target.name]: 'Phone number should be 10 digits.' }
        })
      } else {
        setContact({ ...contact, errors: { ...contact.errors, [target.name]: '' } })
      }
    }
    if (target.name === 'Email') {
      if (target.value && !validateEmail(target.value)) {
        setContact({
          ...contact,
          errors: { ...contact.errors, [target.name]: 'Invalid Email' }
        })
      } else {
        setContact({ ...contact, errors: { ...contact.errors, [target.name]: '' } })
      }
    }
  }

  async function onSubmit() {
    setContact({ ...contact, errorShow: true })
    const { Name, Email, Phone, Message } = contact.values
    const errors = contact.errors
    if (Object.keys(errors).every((k) => !errors[k])) {
      const data = {
        fullName: Name,
        email: Email,
        phoneNumber: Phone,
        message: Message
      }

      const source = cancelToken().source()

      try {
        let url = `${config.baseUrl}/utilities/contact_us`
        const res = await postAuthorized(url, data, source.token)
        if (res?.data?.error) {
          setContact({ ...contact, error: true, msg: res?.data?.message })
        } else {
          setContact({
            ...contact,
            error: false,
            msg: res?.data?.message,
            errors: {
              Phone: '',
              Name: '',
              Email: '',
              Message: ''
            },
            values: {
              Phone: '',
              Name: '',
              Email: '',
              Message: ''
            }
          })
        }
      } catch (error) {
        setContact({ ...contact, error: true, msg: error })
      }
    }
  }

  const isSubmitEnabled =
    contact.values.Name && contact.values.Phone && contact.values.Email && contact.values.Message
  return modal ? (
    <Modal variant="m Left-Center" toggleModal={modal} setToggleModal={() => setModal(false)}>
      <ContactUsContainer>
        <ContactInfo>
          <a href="mailto:info.jump2join@gmail.com">
            <LabelValue label="Email" value="info.jump2join@gmail.com" icon={<Email size={35} />} />
          </a>
          <a href="https://wa.link/i95nz6" target="_blank">
            <LabelValue label="WhatsApp" value="+91-9027914008" icon={<WhatsApp size={35} />} />
          </a>

          <a href="tel:+91-9027914008">
            <LabelValue label="Phone" value="+91-9027914008" icon={<Phone size={35} />} />
          </a>
          <LabelValue
            label="Address"
            icon={<Map size={35} />}
            value="Jump2Join Training Center, Aliganj Road Tanda Ujjain, Kashipur - 244713"
          />
          <div style={{ textAlign: 'center', color: '#676767' }}>
            <p>Scan QR to connect on WhatsApp</p>
            <img
              src="https://pikpart.s3.ap-south-1.amazonaws.com/Jump2Join/assets/WhatsApp+Image+2021-06-26+at+16.25.50.jpeg"
              width="86"
              height="86"
            />
          </div>
        </ContactInfo>
        <ContactInfo>
          <div
            style={{
              textAlign: 'center',
              fontWeight: 'bold',
              textDecoration: 'underline',
              color: '#1890ff !important',
              fontSize: 14
            }}
          >
            DROP US A LINE
          </div>
          <div style={{ margin: '10px 0px' }}>
            <FormInput
              label="Name*"
              type="text"
              placeholder="Full Name"
              name="Name"
              onChange={onchange}
              onBlur={onBlur}
              value={contact.values['Name']}
              fieldErrors={contact.errors}
              style={{ formElement: { fontSize: '12px', fontWeight: 200 } }}
            />
            <FormInput
              label="Phone*"
              type="number"
              placeholder="Phone Number"
              name="Phone"
              onChange={onchange}
              onBlur={onBlur}
              value={contact.values['Phone']}
              fieldErrors={contact.errorShow && contact.errors}
              style={{ formElement: { fontSize: '12px', fontWeight: 200 } }}
            />
            <FormInput
              label="Email*"
              type="email"
              placeholder="Email"
              name="Email"
              onChange={onchange}
              onBlur={onBlur}
              value={contact.values['Email']}
              fieldErrors={contact.errorShow && contact.errors}
              style={{ formElement: { fontSize: '12px', fontWeight: 200 } }}
            />
            <FormInput
              label="Message*"
              type="textarea"
              placeholder="Message"
              name="Message"
              onChange={onchange}
              onBlur={onBlur}
              value={contact.values['Message']}
              fieldErrors={contact.errors}
              style={{ formElement: { fontSize: '12px', fontWeight: 400 } }}
            />
          </div>
          <div>
            <Button
              variant="m primary"
              disabled={!isSubmitEnabled}
              style={{ margin: '0 auto' }}
              onClick={onSubmit}
            >
              CONTACT US
            </Button>
          </div>
        </ContactInfo>
      </ContactUsContainer>
    </Modal>
  ) : null
}

export default ContactUs
