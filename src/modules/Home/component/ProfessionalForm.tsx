import React from 'react'
import { Flex, Button, styled, Modal } from '@icstark/ui'
import { cancelToken } from '@icstark/utils'
import { FormInput } from '../../../components/Forms/Form'
import { FaHandshake } from 'react-icons/fa'
import { FormContainer } from './CertificateForm'
import { postAuthorized } from '../../../services'
import { config } from '../../../config/config'
import CheckBox from './CheckBox'
import { validateEmail } from '../../../utility/func'
import { AvatarContainer } from '../styled'

function ProfessionalForm() {
  const [modal, setModal] = React.useState({ show: false })
  const [loading, setLoading] = React.useState(false)
  const [form, setForm] = React.useState<any>({
    values: {},
    errors: {},
    checkbox: {},
    errorShow: false,
    error: false,
    msg: ''
  })

  const CandidateSelection = styled(Flex)`
    width: 100%;
    // display: none;
  `
  const CandidateNumber = styled(Flex)`
    flex-direction: column;
    align-items: center;
    width: 30px;
  `
  function onchange(target: any) {
    setForm({
      ...form,
      values: { ...form.values, [target.name]: target.value }
    })
  }
  function onBlur({ target }: any) {
    if (target.name === 'RepresentativePhone') {
      if (target.value.length !== 10) {
        setForm({
          ...form,
          errors: { ...form.errors, [target.name]: 'Phone number should be 10 digits.' }
        })
      } else {
        setForm({ ...form, errors: { ...form.errors, [target.name]: '' } })
      }
    }
    if (target.name === 'CompanyMail') {
      if (target.value && !validateEmail(target.value)) {
        setForm({
          ...form,
          errors: { ...form.errors, [target.name]: 'Invalid Email' }
        })
      } else {
        setForm({ ...form, errors: { ...form.errors, [target.name]: '' } })
      }
    }
    if (target.name === 'MechanicBasic') {
      if (target.value < 1 && form.checkbox.MechanicBasic1) {
        setForm({
          ...form,
          values: { ...form.values, [target.name]: '1' }
        })
      } else {
        setForm({ ...form, errors: { ...form.errors, [target.name]: '' } })
      }
    }
  }
  async function onSubmit() {
    setLoading(true)
    const {
      CompanyName,
      CompanyMail,
      RepresentativeName,
      RepresentativePhone,
      MechanicBasic,
      MechanicAdvance,
      MechanicExpert,
      EVBasic,
      EVAdvance,
      EVExpert,
      DataEntryBasic,
      DataEntryAdvance,
      DataEntryExpert,
      SupervisorBasic,
      SupervisorAdvance,
      SupervisorExpert
    } = form.values
    const errors = form.errors
    setForm({ ...form, errorShow: true })
    const source = cancelToken().source()
    const data = {
      companyName: CompanyName,
      companyEmail: CompanyMail,
      representativeName: RepresentativeName,
      representativePhoneNumber: RepresentativePhone
    }
    if (Object.keys(errors).every((k) => !errors[k])) {
      try {
        let url = `${config.baseUrl}/utilities/request_certificate`
        const res = await postAuthorized(url, data, source.token)
        if (res?.data?.error) {
          setForm({ ...form, error: true, msg: res?.data?.message })
        } else {
          setModal({ show: false })
          setForm({
            ...form,
            error: false,
            msg: res?.data?.message,
            errors: {
              RepresentativePhone: '',
              CompanyName: '',
              CompanyMail: '',
              RepresentativeName: ''
              // MechanicBasic: '',
              // MechanicAdvance: '',
              // MechanicExpert: '',
              // EVBasic: '',
              // EVAdvance: '',
              // EVExpert: '',
              // DataEntryBasic: '',
              // DataEntryAdvance: '',
              // DataEntryExpert: '',
              // SupervisorBasic: '',
              // SupervisorAdvance: '',
              // SupervisorExpert: ''
            },
            values: {
              RepresentativePhone: '',
              CompanyName: '',
              CompanyMail: '',
              RepresentativeName: ''
              // MechanicBasic: '',
              // MechanicAdvance: '',
              // MechanicExpert: '',
              // EVBasic: '',
              // EVAdvance: '',
              // EVExpert: '',
              // DataEntryBasic: '',
              // DataEntryAdvance: '',
              // DataEntryExpert: '',
              // SupervisorBasic: '',
              // SupervisorAdvance: '',
              // SupervisorExpert: ''
            }
          })
        }
        setLoading(false)
      } catch (error) {
        setLoading(false)
        setForm({ ...form, error: true, msg: error })
      }
    }
  }
  const isSubmitEnabled =
    form.values.CompanyName &&
    form.values.CompanyMail &&
    form.values.RepresentativeName &&
    form.values.RepresentativePhone
  // &&
  // (form.checkbox.MechanicBasicCheck ||
  //   form.checkbox.MechanicAdvanceCheck ||
  //   form.checkbox.MechanicExpertCheck ||
  //   form.checkbox.EVBasicCheck ||
  //   form.checkbox.EVAdvanceCheck ||
  //   form.checkbox.EVExpertCheck ||
  //   form.checkbox.DataEntryBasicCheck ||
  //   form.checkbox.DataEntryAdvanceCheck ||
  //   form.checkbox.DataEntryExpertCheck ||
  //   form.checkbox.SupervisorBasicCheck ||
  //   form.checkbox.SupervisorAdvanceCheck ||
  //   form.checkbox.SupervisorExpertCheck)

  return (
    <>
      <AvatarContainer column alignItemsCenter onClick={() => setModal({ show: true })}>
        <FaHandshake style={{ fontSize: '45px', marginBottom: '20px' }} />
        <div style={{ fontWeight: 500, fontSize: '14px', textAlign: 'center' }}>
          Request
          <br />
          Certified Professional
        </div>
      </AvatarContainer>
      <Modal
        variant="xl Left-Center"
        toggleModal={modal.show}
        style={{ content: { minHeight: '90vh' } }}
        setToggleModal={() => setModal({ ...modal, show: false })}
      >
        <FormContainer>
          <Flex width={[1, 0.8]} column>
            <div
              style={{
                textAlign: 'center',
                textDecoration: 'underline',
                fontSize: 14
              }}
            >
              Request Certified Professional Form
            </div>
            <Flex style={{ margin: '10px 0px' }} wrap justifyContentSpaceBetween alignItemsCenter>
              <Flex width={[1, 0.47]}>
                <FormInput
                  label="Company Name*"
                  type="text"
                  placeholder="Company"
                  name="CompanyName"
                  onChange={onchange}
                  onBlur={onBlur}
                  value={form.values['CompanyName']}
                  fieldErrors={form.errors}
                  style={{ formElement: { fontSize: '12px', fontWeight: 200 } }}
                />
              </Flex>
              <Flex width={[1, 0.47]}>
                <FormInput
                  label="Email*"
                  type="email"
                  placeholder="Company Email Address"
                  name="CompanyMail"
                  onChange={onchange}
                  onBlur={onBlur}
                  value={form.values['CompanyMail']}
                  fieldErrors={form.errorShow && form.errors}
                  style={{ formElement: { fontSize: '12px', fontWeight: 200 } }}
                />
              </Flex>
              <Flex width={[1, 0.47]}>
                <FormInput
                  label="Representative Name*"
                  type="text"
                  placeholder="Person Who Represent the Company"
                  name="RepresentativeName"
                  onChange={onchange}
                  onBlur={onBlur}
                  value={form.values['RepresentativeName']}
                  fieldErrors={form.errors}
                  style={{ formElement: { fontSize: '12px', fontWeight: 200 } }}
                />
              </Flex>
              <Flex width={[1, 0.47]}>
                <FormInput
                  label="Representative Phone*"
                  type="number"
                  placeholder="Phone Number of the Representative"
                  name="RepresentativePhone"
                  onChange={onchange}
                  onBlur={onBlur}
                  value={form.values['RepresentativePhone']}
                  fieldErrors={form.errorShow && form.errors}
                  style={{ formElement: { fontSize: '12px', fontWeight: 200 } }}
                />
              </Flex>
              {/*<CandidateSelection column>
                <Flex width={[1]} style={{ padding: '30px 0px 20px 0px' }}>
                  <Flex width={[0.3]}>
                    <div style={{ fontSize: '12px', color: '#676767' }}>Candidate Selection*</div>
                  </Flex>
                  <Flex width={[0.7]} justifyContentSpaceAround>
                    <div style={{ fontSize: '12px', color: '#676767' }}>
                      Check The Type(s) of Professional You Need And Enter the Number of
                      Professional
                    </div>
                  </Flex>
                </Flex>
                <Flex width={[1]}>
                  <Flex width={[0.3]} alignItemsCenter>
                    <div style={{ fontSize: '12px', color: '#676767' }}>Two Wheeler Mechanic</div>
                  </Flex>
                  <Flex width={[0.7]} justifyContentSpaceBetween>
                    <CandidateNumber>
                      <div style={{ fontSize: '12px', color: '#676767', padding: '5px 0px' }}>
                        Basic
                      </div>
                      <CheckBox
                        name="MechanicBasicCheck"
                        checked={form.checkbox['MechanicBasicCheck']}
                        onChange={({ target }: any) => {
                          console.log(target.name)
                          setForm({
                            ...form,
                            checkbox: { ...form.checkbox, MechanicBasicCheck: target.checked }
                          })
                        }}
                      />
                      <FormInput
                        type="number"
                        name="MechanicBasic"
                        placeholder="0-5"
                        onChange={onchange}
                        onBlur={onBlur}
                        value={form.values['MechanicBasic']}
                        fieldErrors={form.errorShow && form.errors}
                        style={{
                          container: { width: 40, flexGrow: 0 },
                          formElement: { fontSize: '12px', fontWeight: 200 }
                        }}
                      />
                    </CandidateNumber>
                    <CandidateNumber>
                      <div style={{ fontSize: '12px', color: '#676767', padding: '5px 0px' }}>
                        Advance
                      </div>
                      <CheckBox
                        name="MechanicAdvanceCheck"
                        checked={form.checkbox['MechanicAdvanceCheck']}
                        onChange={({ target }: any) => {
                          console.log(target.name)
                          setForm({
                            ...form,
                            checkbox: { ...form.checkbox, MechanicAdvanceCheck: target.checked }
                          })
                        }}
                      />
                      <FormInput
                        type="number"
                        placeholder="0-5"
                        name="MechanicAdvance"
                        onChange={onchange}
                        onBlur={onBlur}
                        value={form.values['MechanicAdvance']}
                        fieldErrors={form.errors}
                        style={{
                          container: { width: 40, flexGrow: 0 },
                          formElement: { fontSize: '12px', fontWeight: 200 }
                        }}
                      />
                    </CandidateNumber>
                    <CandidateNumber>
                      <div style={{ fontSize: '12px', color: '#676767', padding: '5px 0px' }}>
                        Expert
                      </div>
                      <CheckBox
                        name="MechanicExpertCheck"
                        checked={form.checkbox['MechanicExpertCheck']}
                        onChange={({ target }: any) => {
                          console.log(target.name)
                          setForm({
                            ...form,
                            checkbox: { ...form.checkbox, MechanicExpertCheck: target.checked }
                          })
                        }}
                      />
                      <FormInput
                        type="number"
                        placeholder="0-5"
                        name="MechanicExpert"
                        onChange={onchange}
                        onBlur={onBlur}
                        value={form.values['MechanicExpert']}
                        fieldErrors={form.errors}
                        style={{
                          container: { width: 40, flexGrow: 0 },
                          formElement: { fontSize: '12px', fontWeight: 200 }
                        }}
                      />
                    </CandidateNumber>
                  </Flex>
                </Flex>
                <Flex width={[1]}>
                  <Flex width={[0.3]} alignItemsCenter>
                    <div style={{ fontSize: '12px', color: '#676767' }}>
                      Electric Vehicle Mechanic
                    </div>
                  </Flex>
                  <Flex width={[0.7]} justifyContentSpaceBetween>
                    <CandidateNumber>
                      <div style={{ fontSize: '12px', color: '#676767', padding: '5px 0px' }}>
                        Basic
                      </div>
                      <CheckBox
                        name="EVBasicCheck"
                        checked={form.checkbox['EVBasicCheck']}
                        onChange={({ target }: any) => {
                          console.log(target.name)
                          setForm({
                            ...form,
                            checkbox: { ...form.checkbox, EVBasicCheck: target.checked }
                          })
                        }}
                      >
                        <FormInput
                          type="number"
                          name="EVBasic"
                          placeholder="0-5"
                          onChange={onchange}
                          onBlur={onBlur}
                          value={form.values['EVBasic']}
                          fieldErrors={form.errors}
                          style={{
                            container: { width: 40, flexGrow: 0 },
                            formElement: { fontSize: '12px', fontWeight: 200 }
                          }}
                        />
                      </CheckBox>
                    </CandidateNumber>
                    <CandidateNumber>
                      <div style={{ fontSize: '12px', color: '#676767', padding: '5px 0px' }}>
                        Advance
                      </div>
                      <CheckBox
                        name="EVAdvanceCheck"
                        checked={form.checkbox['EVAdvanceCheck']}
                        onChange={({ target }: any) => {
                          console.log(target.name)
                          setForm({
                            ...form,
                            checkbox: { ...form.checkbox, EVAdvanceCheck: target.checked }
                          })
                        }}
                      >
                        <FormInput
                          type="number"
                          placeholder="0-5"
                          name="EVAdvance"
                          onChange={onchange}
                          onBlur={onBlur}
                          value={form.values['EVAdvance']}
                          fieldErrors={form.errors}
                          style={{
                            container: { width: 40, flexGrow: 0 },
                            formElement: { fontSize: '12px', fontWeight: 200 }
                          }}
                        />
                      </CheckBox>
                    </CandidateNumber>
                    <CandidateNumber>
                      <div style={{ fontSize: '12px', color: '#676767', padding: '5px 0px' }}>
                        Expert
                      </div>
                      <CheckBox
                        name="EVExpertCheck"
                        checked={form.checkbox['EVExpertCheck']}
                        onChange={({ target }: any) => {
                          console.log(target.name)
                          setForm({
                            ...form,
                            checkbox: { ...form.checkbox, EVExpertCheck: target.checked }
                          })
                        }}
                      >
                        <FormInput
                          type="number"
                          placeholder="0-5"
                          name="EVExpert"
                          onChange={onchange}
                          onBlur={onBlur}
                          value={form.values['EVExpert']}
                          fieldErrors={form.errors}
                          style={{
                            container: { width: 40, flexGrow: 0 },
                            formElement: { fontSize: '12px', fontWeight: 200 }
                          }}
                        />
                      </CheckBox>
                    </CandidateNumber>
                  </Flex>
                </Flex>
                <Flex width={[1]}>
                  <Flex width={[0.3]} alignItemsCenter>
                    <div style={{ fontSize: '12px', color: '#676767' }}>Data Entry Operator</div>
                  </Flex>
                  <Flex width={[0.7]} justifyContentSpaceBetween>
                    <CandidateNumber>
                      <div style={{ fontSize: '12px', color: '#676767', padding: '5px 0px' }}>
                        Basic
                      </div>
                      <CheckBox
                        name="DataEntryBasicCheck"
                        checked={form.checkbox['DataEntryBasicCheck']}
                        onChange={({ target }: any) => {
                          console.log(target.name)
                          setForm({
                            ...form,
                            checkbox: { ...form.checkbox, DataEntryBasicCheck: target.checked }
                          })
                        }}
                      >
                        <FormInput
                          type="number"
                          name="DataEntryBasic"
                          placeholder="0-5"
                          onChange={onchange}
                          onBlur={onBlur}
                          value={form.values['DataEntryBasic']}
                          fieldErrors={form.errors}
                          style={{
                            container: { width: 40, flexGrow: 0 },
                            formElement: { fontSize: '12px', fontWeight: 200 }
                          }}
                        />
                      </CheckBox>
                    </CandidateNumber>
                    <CandidateNumber>
                      <div style={{ fontSize: '12px', color: '#676767', padding: '5px 0px' }}>
                        Advance
                      </div>
                      <CheckBox
                        name="DataEntryAdvanceCheck"
                        checked={form.checkbox['DataEntryAdvanceCheck']}
                        onChange={({ target }: any) => {
                          console.log(target.name)
                          setForm({
                            ...form,
                            checkbox: { ...form.checkbox, DataEntryAdvanceCheck: target.checked }
                          })
                        }}
                      >
                        <FormInput
                          type="number"
                          placeholder="0-5"
                          name="DataEntryAdvance"
                          onChange={onchange}
                          onBlur={onBlur}
                          value={form.values['DataEntryAdvance']}
                          fieldErrors={form.errors}
                          style={{
                            container: { width: 40, flexGrow: 0 },
                            formElement: { fontSize: '12px', fontWeight: 200 }
                          }}
                        />
                      </CheckBox>
                    </CandidateNumber>
                    <CandidateNumber>
                      <div style={{ fontSize: '12px', color: '#676767', padding: '5px 0px' }}>
                        Expert
                      </div>
                      <CheckBox
                        name="DataEntryExpertCheck"
                        checked={form.checkbox['DataEntryExpertCheck']}
                        onChange={({ target }: any) => {
                          console.log(target.name)
                          setForm({
                            ...form,
                            checkbox: { ...form.checkbox, DataEntryExpertCheck: target.checked }
                          })
                        }}
                      >
                        <FormInput
                          type="number"
                          placeholder="0-5"
                          name="DataEntryExpert"
                          onChange={onchange}
                          onBlur={onBlur}
                          value={form.values['DataEntryExpert']}
                          fieldErrors={form.errors}
                          style={{
                            container: { width: 40, flexGrow: 0 },
                            formElement: { fontSize: '12px', fontWeight: 200 }
                          }}
                        />
                      </CheckBox>
                    </CandidateNumber>
                  </Flex>
                </Flex>
                <Flex width={[1]}>
                  <Flex width={[0.3]} alignItemsCenter>
                    <div style={{ fontSize: '12px', color: '#676767' }}>Maintenance Supervisor</div>
                  </Flex>
                  <Flex width={[0.7]} justifyContentSpaceBetween>
                    <CandidateNumber>
                      <div style={{ fontSize: '12px', color: '#676767', padding: '5px 0px' }}>
                        Basic
                      </div>
                      <CheckBox
                        name="SupervisorBasicCheck"
                        checked={form.checkbox['SupervisorBasicCheck']}
                        onChange={({ target }: any) => {
                          console.log(target.name)
                          setForm({
                            ...form,
                            checkbox: { ...form.checkbox, SupervisorBasicCheck: target.checked }
                          })
                        }}
                      >
                        <FormInput
                          type="number"
                          name="SupervisorBasic"
                          placeholder="0-5"
                          onChange={onchange}
                          onBlur={onBlur}
                          value={form.values['SupervisorBasic']}
                          fieldErrors={form.errors}
                          style={{
                            container: { width: 40, flexGrow: 0 },
                            formElement: { fontSize: '12px', fontWeight: 200 }
                          }}
                        />
                      </CheckBox>
                    </CandidateNumber>
                    <CandidateNumber>
                      <div style={{ fontSize: '12px', color: '#676767', padding: '5px 0px' }}>
                        Advance
                      </div>
                      <CheckBox
                        name="SupervisorAdvanceCheck"
                        checked={form.checkbox['SupervisorAdvanceCheck']}
                        onChange={({ target }: any) => {
                          console.log(target.name)
                          setForm({
                            ...form,
                            checkbox: { ...form.checkbox, SupervisorAdvanceCheck: target.checked }
                          })
                        }}
                      >
                        <FormInput
                          type="number"
                          placeholder="0-5"
                          name="SupervisorAdvance"
                          onChange={onchange}
                          onBlur={onBlur}
                          value={form.values['SupervisorAdvance']}
                          fieldErrors={form.errors}
                          style={{
                            container: { width: 40, flexGrow: 0 },
                            formElement: { fontSize: '12px', fontWeight: 200 }
                          }}
                        />
                      </CheckBox>
                    </CandidateNumber>
                    <CandidateNumber>
                      <div style={{ fontSize: '12px', color: '#676767', padding: '5px 0px' }}>
                        Expert
                      </div>
                      <CheckBox
                        name="SupervisorExpertCheck"
                        checked={form.checkbox['SupervisorExpertCheck']}
                        onChange={({ target }: any) => {
                          console.log(target.name)
                          setForm({
                            ...form,
                            checkbox: { ...form.checkbox, SupervisorExpertCheck: target.checked }
                          })
                        }}
                      >
                        <FormInput
                          type="number"
                          placeholder="0-5"
                          name="SupervisorExpert"
                          onChange={onchange}
                          onBlur={onBlur}
                          value={form.values['SupervisorExpert']}
                          fieldErrors={form.errors}
                          style={{
                            container: { width: 40, flexGrow: 0 },
                            formElement: { fontSize: '12px', fontWeight: 200 }
                          }}
                        />
                      </CheckBox>
                    </CandidateNumber>
                  </Flex>
                </Flex>
              </CandidateSelection>*/}
            </Flex>
            <Button
              variant="m primary"
              isLoading={loading}
              disabled={!isSubmitEnabled}
              style={{ margin: '0 auto' }}
              onClick={onSubmit}
            >
              REGISTER
            </Button>
          </Flex>
        </FormContainer>
      </Modal>
    </>
  )
}
export default ProfessionalForm
