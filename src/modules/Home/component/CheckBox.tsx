import React from 'react'
import { Flex, Input } from '@icstark/ui'

function CheckBox({ children, onChange, checked, name }: any) {
  return (
    <Flex column alignItemsCenter>
      <Input type="checkbox" name={name} variant="l" checked={checked} onChange={onChange} />
      {checked && children}
    </Flex>
  )
}

export default CheckBox
