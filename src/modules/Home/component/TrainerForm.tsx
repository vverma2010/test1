import React from 'react'
import { Flex, Span, Button } from '@icstark/ui'
import { Modal } from '@icstark/ui'
import { FormInput, FormSelect } from '../../../components/Forms/Form'
import { FaUser, FaUserTie } from 'react-icons/fa'
import { FormContainer } from './CertificateForm'
import { config } from '../../../config/config'
import { postAuthorized } from '../../../services'
import { cancelToken } from '@icstark/utils'
import { validateEmail } from '../../../utility/func'
import { AvatarContainer } from '../styled'

function TrainerForm() {
  const [modal, setModal] = React.useState({ show: false })
  const [loading, setLoading] = React.useState(false)
  const [form, setForm] = React.useState<any>({
    values: {
      Name: '',
      Email: '',
      Phone: '',
      Address: '',
      Location: '',
      Qualification: '',
      WorkExperience: '',
      Expertise: ''
    },
    errors: {},
    errorShow: false,
    error: false,
    msg: ''
  })
  function onchange(target: any) {
    setForm({
      ...form,
      values: { ...form.values, [target.name]: target.value }
    })
  }
  function onBlur({ target }: any) {
    if (target.name === 'Phone') {
      if (target.value.length !== 10) {
        setForm({
          ...form,
          errors: { ...form.errors, [target.name]: 'Phone number should be 10 digits.' }
        })
      } else {
        setForm({ ...form, errors: { ...form.errors, [target.name]: '' } })
      }
    }
    if (target.name === 'Email') {
      if (target.value && !validateEmail(target.value)) {
        setForm({
          ...form,
          errors: { ...form.errors, [target.name]: 'Invalid Email' }
        })
      } else {
        setForm({ ...form, errors: { ...form.errors, [target.name]: '' } })
      }
    }
  }
  async function onSubmit() {
    setLoading(true)
    const {
      Name,
      Email,
      Phone,
      Address,
      Qualification,
      WorkExperience,
      Company,
      Position,
      Experience,
      Expertise,
      Location
    } = form.values
    const errors = form.errors
    setForm({ ...form, errorShow: true })
    const data = {
      fullName: Name,
      email: Email,
      phoneNumber: Phone,
      address: Address,
      location: Location,
      qualification: Qualification,
      workExperience: WorkExperience,
      experience: Experience,
      expertise: Expertise,
      companyName: Company,
      position: Position
    }
    const source = cancelToken().source()
    if (Object.keys(errors).every((k) => !errors[k])) {
      try {
        let url = `${config.baseUrl}/utilities/trainer_registration`

        const res = await postAuthorized(url, data, source.token)
        if (res?.data?.error) {
          setForm({ ...form, error: true, msg: res?.data?.message })
        } else {
          setForm({
            ...form,
            error: false,
            msg: res?.data?.message,
            errors: {
              Phone: '',
              Name: '',
              Email: '',
              Address: '',
              Location: '',
              Qualification: '',
              WorkExperience: '',
              Expertise: ''
            },
            values: {
              Phone: '',
              Name: '',
              Email: '',
              Address: '',
              Location: '',
              Qualification: '',
              WorkExperience: '',
              Expertise: ''
            }
          })
          setModal({ show: false })
        }
        setLoading(false)
      } catch (error) {
        setLoading(false)
        setForm({ ...form, error: true, msg: error })
      }
    }
  }
  const isSubmitEnabled =
    form.values.Name &&
    form.values.Email &&
    form.values.Phone &&
    form.values.Address &&
    form.values.Location &&
    form.values.Qualification &&
    form.values.WorkExperience &&
    form.values.Expertise
  return (
    <>
      <AvatarContainer column alignItemsCenter onClick={() => setModal({ show: true })}>
        <FaUserTie style={{ fontSize: '60px', marginBottom: '20px' }} />
        <div style={{ fontWeight: 500, fontSize: '14px' }}>Apply For Trainer</div>
      </AvatarContainer>
      <Modal
        variant="xl Left-Center"
        toggleModal={modal.show}
        style={{ content: { minHeight: '75vh' } }}
        setToggleModal={() => setModal({ show: false })}
      >
        <FormContainer>
          <Flex width={[1, 0.8]} column>
            <div
              style={{
                textAlign: 'center',
                textDecoration: 'underline',
                fontSize: 14
              }}
            >
              Trainer Registration Form
            </div>
            <Flex style={{ margin: '10px 0px' }} wrap justifyContentSpaceBetween alignItemsCenter>
              <Flex width={[1, 0.47]}>
                <FormInput
                  label="Full Name*"
                  type="text"
                  placeholder="John Doe"
                  name="Name"
                  onChange={onchange}
                  onBlur={onBlur}
                  value={form.values['Name']}
                  fieldErrors={form.errors}
                  style={{ formElement: { fontSize: '12px', fontWeight: 200 } }}
                />
              </Flex>
              <Flex width={[1, 0.47]}>
                <FormInput
                  label="Email*"
                  type="email"
                  placeholder="abc@xyz.com"
                  name="Email"
                  onChange={onchange}
                  onBlur={onBlur}
                  value={form.values['Email']}
                  fieldErrors={form.errorShow && form.errors}
                  style={{ formElement: { fontSize: '12px', fontWeight: 200 } }}
                />
              </Flex>
              <Flex width={[1, 0.47]}>
                <FormInput
                  label="Phone Number*"
                  type="number"
                  placeholder="9999999999"
                  name="Phone"
                  onChange={onchange}
                  onBlur={onBlur}
                  value={form.values['Phone']}
                  fieldErrors={form.errorShow && form.errors}
                  style={{ formElement: { fontSize: '12px', fontWeight: 200 } }}
                />
              </Flex>
              <Flex width={[1, 0.47]}>
                <FormInput
                  label="Address*"
                  type="textarea"
                  placeholder="Enter full Address"
                  name="Address"
                  onChange={onchange}
                  onBlur={onBlur}
                  value={form.values['Address']}
                  fieldErrors={form.errors}
                  style={{ formElement: { fontSize: '12px', fontWeight: 400 } }}
                />
              </Flex>
              <Flex width={[1, 0.47]}>
                <FormSelect
                  placeholder="Select Your Choice"
                  name="Qualification"
                  label="Academic Qualification*"
                  onChange={onchange}
                  onBlur={onBlur}
                  list={[
                    {
                      label: 'Diploma',
                      value: 'Diploma'
                    },
                    {
                      label: 'ITI',
                      value: 'ITI'
                    },
                    {
                      label: 'B-Tech',
                      value: 'B-Tech'
                    }
                  ]}
                  value={form.values['Qualification']}
                  fieldErrors={form.errors}
                  style={{
                    formElement: {
                      fontSize: '12px',
                      fontWeight: 200,
                      border: '1px solid #d9d9d9',
                      borderRadius: 3,
                      color: '#676767'
                    }
                  }}
                />
              </Flex>
              <Flex width={[1, 0.47]}>
                <FormSelect
                  placeholder="Select Your Choice"
                  name="WorkExperience"
                  label="Have Work Experience?*"
                  onChange={onchange}
                  onBlur={onBlur}
                  list={[
                    {
                      label: 'Yes',
                      value: 'yes'
                    },
                    {
                      label: 'No',
                      value: 'no'
                    }
                  ]}
                  value={form.values['WorkExperience']}
                  fieldErrors={form.errors}
                  style={{
                    formElement: {
                      fontSize: '12px',
                      fontWeight: 200,
                      border: '1px solid #d9d9d9',
                      borderRadius: 3,
                      color: '#676767'
                    }
                  }}
                />
              </Flex>
              {form.values.WorkExperience === 'yes' ? (
                <>
                  <Flex width={[1, 0.47]}>
                    <FormInput
                      label="Company Name*"
                      type="text"
                      placeholder="Company Name"
                      name="Company"
                      onChange={onchange}
                      onBlur={onBlur}
                      value={form.values['Company']}
                      fieldErrors={form.errors}
                      style={{ formElement: { fontSize: '12px', fontWeight: 200 } }}
                    />
                  </Flex>
                  <Flex width={[1, 0.47]}>
                    <FormInput
                      label="Position*"
                      type="text"
                      placeholder="Working Position In The Company"
                      name="Position"
                      onChange={onchange}
                      onBlur={onBlur}
                      value={form.values['Position']}
                      fieldErrors={form.errors}
                      style={{ formElement: { fontSize: '12px', fontWeight: 200 } }}
                    />
                  </Flex>
                  <Flex width={[1, 0.47]}>
                    <FormInput
                      label="Duration*"
                      type="number"
                      placeholder="Working Duration With The Company In month"
                      name="Experience"
                      onChange={onchange}
                      onBlur={onBlur}
                      value={form.values['Experience']}
                      fieldErrors={form.errors}
                      style={{ formElement: { fontSize: '12px', fontWeight: 200 } }}
                    />
                  </Flex>
                </>
              ) : null}
              <Flex width={[1, 0.47]}>
                <FormInput
                  label="Area of Expertise*"
                  type="textarea"
                  placeholder="Enter The Topics In Which You Have Good Knowledge"
                  name="Expertise"
                  onChange={onchange}
                  onBlur={onBlur}
                  value={form.values['Expertise']}
                  fieldErrors={form.errors}
                  style={{ formElement: { fontSize: '12px', fontWeight: 400 } }}
                />
              </Flex>
              <Flex width={[1, 0.47]}>
                <FormSelect
                  placeholder="Select Your Choice"
                  name="Location"
                  label="Preferred Location*"
                  onChange={onchange}
                  onBlur={onBlur}
                  list={[{ label: 'Kashipur', value: 'Kashipur' }]}
                  value={form.values['Location']}
                  fieldErrors={form.errors}
                  style={{
                    formElement: {
                      fontSize: '12px',
                      fontWeight: 200,
                      border: '1px solid #d9d9d9',
                      borderRadius: 3,
                      color: '#676767'
                    }
                  }}
                />
              </Flex>
            </Flex>
            <Button
              variant="m primary"
              disabled={!isSubmitEnabled}
              style={{ margin: '0 auto' }}
              onClick={onSubmit}
              isLoading={loading}
            >
              REGISTER
            </Button>
          </Flex>
        </FormContainer>
      </Modal>
    </>
  )
}
export default TrainerForm
