import React from 'react'
import { Flex, Span, Button, styled } from '@icstark/ui'
import { FaCertificate, FaGraduationCap, FaRegBell } from 'react-icons/fa'
import { Modal } from '@icstark/ui'
import { FormInput, FormSelect } from '../../../components/Forms/Form'
import { config } from '../../../config/config'
import { postAuthorized } from '../../../services'
import { cancelToken } from '@icstark/utils'
import { validateEmail } from '../../../utility/func'
import { AvatarContainer } from '../styled'

export const FormContainer = styled(Flex)`
  justify-content: center;
  align-items: center;
  width: 95%;
  margin: 40px auto;
  @media (max-width: 540px) {
    flex-wrap: wrap;
  }
`

function CertificateForm(props: any) {
  const [modal, setModal] = React.useState({
    show: false
  })
  const [loading, setLoading] = React.useState(false)
  const [form, setForm] = React.useState<any>({
    values: {
      // Course: 'Diploma Course in Two Wheeler Mechanism and Maintenance'
      Name: '',
      Phone: '',
      Email: '',
      Center: '',
      Course: '',
      Qualification: '',
      Percent: '',
      Experience: '',
      Message: ''
    },
    errors: {},
    errorShow: false,
    error: false,
    msg: ''
  })
  function onchange(target: any) {
    setForm({
      ...form,
      values: { ...form.values, [target.name]: target.value }
    })
  }
  function onBlur({ target }: any) {
    if (target.name === 'Phone') {
      if (target.value.length !== 10) {
        setForm({
          ...form,
          errors: { ...form.errors, [target.name]: 'Phone number should be 10 digits.' }
        })
      } else {
        setForm({ ...form, errors: { ...form.errors, [target.name]: '' } })
      }
    }
    if (target.name === 'Email') {
      if (target.value && !validateEmail(target.value)) {
        setForm({
          ...form,
          errors: { ...form.errors, [target.name]: 'Invalid Email' }
        })
      } else {
        setForm({ ...form, errors: { ...form.errors, [target.name]: '' } })
      }
    }
  }
  async function onSubmit() {
    setLoading(true)
    const {
      Name,
      Email,
      Phone,
      Center,
      Course,
      Percent,
      Experience,
      Qualification,
      Message
    } = form.values
    const errors = form.errors
    setForm({ ...form, errorShow: true })
    const source = cancelToken().source()
    const data = {
      fullName: Name,
      email: Email,
      phoneNumber: Phone,
      trainingCenter: Center,
      courseName: Course,
      qualification: Qualification,
      percent: Percent,
      experience: Experience,
      message: Message
    }
    if (Object.keys(errors).every((k) => !errors[k])) {
      try {
        let url = `${config.baseUrl}/utilities/candidate_registration`
        const res = await postAuthorized(url, data, source.token)
        if (res?.data?.error) {
          setForm({ ...form, error: true, msg: res?.data?.message })
        } else {
          setModal({ show: false })
          setForm({
            ...form,
            error: false,
            msg: res?.data?.message,
            errors: {
              Phone: '',
              Name: '',
              Email: '',
              Center: '',
              Course: '',
              Qualification: '',
              Percent: '',
              Experience: '',
              Message: ''
            },
            values: {
              Phone: '',
              Name: '',
              Email: '',
              Center: '',
              Course: '',
              Qualification: '',
              Percent: '',
              Experience: '',
              Message: ''
            }
          })
        }
        setLoading(false)
      } catch (error) {
        setLoading(false)
        setForm({ ...form, error: true, msg: error })
      }
    }
  }

  const isSubmitEnabled =
    !loading &&
    form.values.Course &&
    form.values.Name &&
    form.values.Email &&
    form.values.Phone &&
    form.values.Center &&
    form.values.Qualification &&
    form.values.Percent &&
    form.values.Experience
  return (
    <>
      <AvatarContainer column alignItemsCenter onClick={() => setModal({ show: true })}>
        <FaGraduationCap
          style={{
            fontSize: '60px',
            marginBottom: '20px'
          }}
        />
        <div style={{ fontWeight: 500, fontSize: '14px' }}>Apply to Get Certified</div>
      </AvatarContainer>
      <Modal
        variant="xl Left-Center"
        toggleModal={modal.show}
        setToggleModal={() => setModal({ ...modal, show: false })}
      >
        <FormContainer>
          <Flex width={[1, 0.8]} column>
            <div
              style={{
                textAlign: 'center',
                textDecoration: 'underline',
                fontSize: 14
              }}
            >
              Candidate Registration Form
            </div>
            <Flex
              style={{
                margin: '10px 0px'
              }}
              wrap
              justifyContentSpaceBetween
              alignItemsCenter
            >
              <Flex width={[1, 0.47]}>
                <FormSelect
                  placeholder="Select Your Choice"
                  name="Course"
                  label="Course Name*"
                  onChange={onchange}
                  onBlur={onBlur}
                  list={[
                    {
                      label: 'Diploma Course in Two Wheeler Mechanism and Maintenance',
                      value: 'Diploma Course in Two Wheeler Mechanism and Maintenance'
                    },
                    {
                      label: 'Training Course In Electric Vehicle Technologies',
                      value: 'Training Course In Electric Vehicle Technologies'
                    },
                    {
                      label: 'Training Course In Data Entry Operator',
                      value: 'Training Course In Data Entry Operator'
                    },
                    {
                      label: 'Training Course In Maintenance Supervisor',
                      value: 'Training Course In Maintenance Supervisor'
                    }
                  ]}
                  value={form.values['Course']}
                  fieldErrors={form.errors}
                  style={{
                    formElement: {
                      fontSize: '12px',
                      fontWeight: 200,
                      border: '1px solid #d9d9d9',
                      borderRadius: 3,
                      color: '#676767'
                    }
                  }}
                />
              </Flex>
              <Flex width={[1, 0.47]}>
                <FormInput
                  label="Full Name*"
                  type="text"
                  placeholder="John Doe"
                  name="Name"
                  onChange={onchange}
                  onBlur={onBlur}
                  value={form.values['Name']}
                  fieldErrors={form.errors}
                  style={{ formElement: { fontSize: '12px', fontWeight: 200 } }}
                />
              </Flex>
              <Flex width={[1, 0.47]}>
                <FormInput
                  label="Email*"
                  type="email"
                  placeholder="abc@xyz.com"
                  name="Email"
                  onChange={onchange}
                  onBlur={onBlur}
                  value={form.values['Email']}
                  fieldErrors={form.errorShow && form.errors}
                  style={{ formElement: { fontSize: '12px', fontWeight: 200 } }}
                />
              </Flex>
              <Flex width={[1, 0.47]}>
                <FormInput
                  label="Phone Number*"
                  type="number"
                  placeholder="9999999999"
                  name="Phone"
                  onChange={onchange}
                  onBlur={onBlur}
                  value={form.values['Phone']}
                  fieldErrors={form.errorShow && form.errors}
                  style={{ formElement: { fontSize: '12px', fontWeight: 200 } }}
                />
              </Flex>
              <Flex width={[1, 0.47]}>
                <FormSelect
                  placeholder="Select Your Choice"
                  name="Center"
                  label="Training Center*"
                  onChange={onchange}
                  onBlur={onBlur}
                  list={[{ label: 'Kashipur', value: 'Kashipur' }]}
                  value={form.values['Center']}
                  fieldErrors={form.errors}
                  style={{
                    formElement: {
                      fontSize: '12px',
                      fontWeight: 200,
                      border: '1px solid #d9d9d9',
                      borderRadius: 3,
                      color: '#676767'
                    }
                  }}
                />
              </Flex>
              <Flex width={[1, 0.47]}>
                <FormSelect
                  placeholder="Select Your Choice"
                  name="Qualification"
                  label="Highest Qualification*"
                  onChange={onchange}
                  onBlur={onBlur}
                  list={[
                    { label: 'Diploma', value: 'Diploma' },
                    { label: 'ITI', value: 'ITI' },
                    { label: 'B-Tech', value: 'B-Tech' }
                  ]}
                  value={form.values['Qualification']}
                  fieldErrors={form.errors}
                  style={{
                    formElement: {
                      fontSize: '12px',
                      fontWeight: 200,
                      border: '1px solid #d9d9d9',
                      borderRadius: 3,
                      color: '#676767'
                    }
                  }}
                />
              </Flex>
              <Flex width={[1, 0.47]}>
                <FormInput
                  label="Percent/CGPA*"
                  type="number"
                  placeholder="Percent/CGPA of Last Semester"
                  name="Percent"
                  onChange={onchange}
                  onBlur={onBlur}
                  value={form.values['Percent']}
                  fieldErrors={form.errors}
                  style={{ formElement: { fontSize: '12px', fontWeight: 200 } }}
                />
              </Flex>
              <Flex width={[1, 0.47]}>
                <FormInput
                  label="Work Experience*"
                  type="number"
                  placeholder="Experience In Months"
                  name="Experience"
                  onChange={onchange}
                  onBlur={onBlur}
                  value={form.values['Experience']}
                  fieldErrors={form.errors}
                  style={{ formElement: { fontSize: '12px', fontWeight: 200 } }}
                />
              </Flex>
              <Flex width={[1, 0.47]}>
                <FormInput
                  label="Message"
                  type="textarea"
                  placeholder="Message for Enquiry"
                  name="Message"
                  onChange={onchange}
                  onBlur={onBlur}
                  value={form.values['Message']}
                  fieldErrors={form.errors}
                  style={{ formElement: { fontSize: '12px', fontWeight: 400 } }}
                />
              </Flex>
            </Flex>

            <Button
              variant="m primary"
              disabled={!isSubmitEnabled}
              isLoading={loading}
              style={{ margin: '0 auto' }}
              onClick={onSubmit}
            >
              REGISTER
            </Button>
          </Flex>
        </FormContainer>
      </Modal>
    </>
  )
}
export default CertificateForm
