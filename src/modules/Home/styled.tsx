import { Flex, styled, Span } from '@icstark/ui'

export const AvatarContainer = styled(Flex)`
  cursor: pointer;
  border: 4px solid #f6b814;
  padding: 40px 0px;
  border-radius: 100px;
  height: 200px;
  width: 200px;
  box-shadow: 0 0 50px -10px #f6b814;
  transition: 0.4s ease;
  &:hover {
    box-shadow: 0 0 20px -5px #f6b814;
    transition: 0.6s ease;
  }
  &:active {
    box-shadow: 0 0 2px -1px #f6b814;
    transition: 0.2s ease;
  }
  @media (max-width: 768px) {
    margin: 10px 0px;
  }
  div,
  svg {
    color: rgba(0, 160, 227, 0.8);
    transition: 0.4s ease;
  }
  &:hover {
    div,
    svg {
      color: rgb(0, 160, 227);
      transition: 0.6s ease;
    }
  }
`

/*============ Certificate ============*/
export const CertificatesContainer = styled(Flex)`
  justify-content: space-around;
  align-items: flex-start;
  @media (max-width: 786px) {
    align-items: center;
    flex-direction: column;
  }
`

export const CertificatesWrapper = styled(Flex)`
  padding: 4vmax 0 2vmax;
  width: 28%;
  img {
    margin: 0 auto;
    display: block;
    height: 100px;
    width: auto;
  }
  @media (max-width: 786px) {
    width: 80%;
  }
`

export const CertificateDesc = styled(Span)`
  font-size: 14px;
  font-weight: 300;
  letter-spacing: 0.3px;
  text-align: justify;
  line-height: 1.6;
`
/*============ Certificate End ============*/
