import { Flex, Span, styled } from '@icstark/ui'
import React from 'react'
import data from '../../../data.json'
import { H2Heading, Section } from '../../../style/styled'
import { CertificateDesc } from '../styled'

function AboutUs() {
  return (
    <Section style={{ padding: '2vmax 0' }}>
      <H2Heading>About Us</H2Heading>
      <Flex justifyContentCenter width={[0.85, 0.6]} style={{ margin: '0 auto' }}>
        <CertificateDesc>{data.aboutUs}</CertificateDesc>
      </Flex>
    </Section>
  )
}

export default AboutUs
