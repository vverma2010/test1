import React from 'react'
import advance from '../../../assets/advance.png'
import basic from '../../../assets/basic.png'
import expert from '../../../assets/expert.png'
import { H3Heading } from '../../../style/styled'
import { CertificateDesc, CertificatesContainer, CertificatesWrapper } from '../styled'

function Certificate() {
  return (
    <CertificatesContainer>
      <CertificatesWrapper column>
        <img src={basic} alt="logo" />
        <H3Heading>Jump2Join Basic Certification</H3Heading>
        <CertificateDesc>
          Jump2Join Basic Certification program comprises training of Basic Tool and equipment of
          relevant industry including the productivity of the same. Professional behavior and work
          ethics are part of it. The Basic Certification program covers onsite internship as well in
          the relevant industry.
        </CertificateDesc>
      </CertificatesWrapper>
      <CertificatesWrapper column>
        <img src={advance} alt="logo" />
        <H3Heading>Jump2Join Advance Certification</H3Heading>
        <CertificateDesc>
          Jump2Join Advance Certification program comprises training of specifically advanced tools
          and equipment of relevant industry and includes the productivity practices of the same.
          Personality development, Basic computer operations training, and onsite internship are
          part of the Advanced Certification program.
        </CertificateDesc>
      </CertificatesWrapper>
      <CertificatesWrapper column>
        <img src={expert} alt="logo" />
        <H3Heading>Jump2Join Expert Certification</H3Heading>
        <CertificateDesc>
          Jump2Join Expert Certification program comprises training of All advanced tools and
          equipment of relevant industry and includes the productivity practices of the same.
          Personality development, Advanced computer operations training, Kaizen Training, Digital
          Marketing Basics, and onsite internship are part of the Expert Certification program.
        </CertificateDesc>
      </CertificatesWrapper>
    </CertificatesContainer>
  )
}
export default Certificate
