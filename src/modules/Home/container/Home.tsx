import React from 'react'
import Banner from '../../../components/Banner/Banner'
import AboutUs from './AboutUs'
import data from '../../../data.json'
// import CourseContent from '../../../modules/CourseContent'
// import ContactRegister from './modules/ContactRegister'
import Feedback from '../../../modules/Feedback/container/Feedback'
import Application from './Application'
import Certificate from './Certificate'
import { SubSection } from '../../../style/styled'
// import ContactUs from '../../../modules/ContactUs/container/ContactUs'

function Home() {
  return (
    <>
      <Banner />
      <SubSection>
        <Certificate />
      </SubSection>
      {/* <AboutUs data={data.aboutUs} /> */}
      {/* <CourseContent data={data.courseContent} /> */}
      <div>
        <AboutUs />
      </div>
      <div>
        <Application />
      </div>
      <div>
        <Feedback />
      </div>
      {/* <ContactRegister courses={data.courses} traningLocations={data.traningLocations} /> */}
      {/* <ContactUs /> */}
    </>
  )
}
export default Home
