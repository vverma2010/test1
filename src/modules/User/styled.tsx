import { Flex, styled } from '@icstark/ui'

export const UserProfile = styled(Flex)`
  align-items: flex-start;
  @media (max-width: 786px) {
    align-items: stretch;
    flex-direction: column;
  }
`
export const Card = styled(Flex)`
  padding: 2vmax;
  border-radius: 10px;
  box-shadow: rgb(204, 204, 204) 0 0 10px;
`

export const Profile = styled(Card)`
  //   position: sticky;
  //   top: 15px;
  width: 100%;
  flex-direction: column;
  @media (max-width: 786px) {
    flex-direction: row;
  }
`
export const Details = styled(Flex)`
  flex-grow: 1;
  text-align: center;
  flex-direction: column;
  @media (max-width: 786px) {
    text-align: left;
    margin-left: 3vw;
  }
`

export const UserCourse = styled(Card)`
  border-radius: 10px;
  box-shadow: rgb(204, 204, 204) 0 0 16px;
  //   background: linear-gradient(to bottom, #eee, #fff);
`
